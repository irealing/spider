#simple spider(java)
===
##简介
* 基于jsoup开发
* 使用xml和注解进行配置
###xml示例
---
```xml
    <?xml version="1.0" encoding="UTF-8"?>
    <spiders>
    <spider url="http://new.sousuo.gov.cn/column/19769/0.htm"
            item="ul.list01 > li > a"
            next=".next">
        <orm class="cn.cnysa.spider.data.Article"
             result="cn.cnysa.spider.data.ShowResult">
            <property name="title" selector=".pages-title"/>
            <property name="content" selector=".pages_content"/>
        </orm>
    </spider>
```