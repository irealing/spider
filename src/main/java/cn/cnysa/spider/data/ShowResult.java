package cn.cnysa.spider.data;

import cn.cnysa.spider.logic.Spider;

/**
 * @author Memory_Leak
 *         Created by ireal on 15-7-9.
 */
public class ShowResult implements Spider.OnResult {
    public void onEnum(Object o) {
        System.out.println(o);
    }
}