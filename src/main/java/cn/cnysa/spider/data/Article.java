package cn.cnysa.spider.data;

/**
 * @author Memory_Leak
 *         Created by ireal on 15-7-7.
 */
public class Article {
    private String title = null;
    private String content = null;

    public String getTitle() {
        return title;
    }

    @OrmFlag("title")
    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    @OrmFlag("content")
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("title:\t");
        builder.append(title);
        builder.append("content:\t");
        builder.append(content);
        return builder.toString();
    }
}