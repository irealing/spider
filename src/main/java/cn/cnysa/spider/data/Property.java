package cn.cnysa.spider.data;

/**
 * @author Memory_Leak
 *         Created by ireal on 15-7-8.
 */
public class Property {
    private String key = null;
    private String value = null;

    public Property(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}