package cn.cnysa.spider;

import cn.cnysa.spider.logic.Spider;

import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 程序入口
 *
 * @author Memory_Leak
 *         Created by ireal on 15-7-9.
 */
public class SpiderApp {
    public static void main(String args[]) {
        ExecutorService service = Executors.newSingleThreadExecutor();
        Iterator<Spider> spiders = Spider.parseXml("spider.xml").iterator();
        while (spiders.hasNext()) {
            Spider spider = spiders.next();
            service.execute(spider);
        }
    }
}