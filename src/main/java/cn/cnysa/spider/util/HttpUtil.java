package cn.cnysa.spider.util;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author Memory_Leak
 *         Created by ireal on 15-7-2.
 */
public class HttpUtil {
    private final static String CONFIG_FILE = "spider.properties";
    @ConfigField("spider.userAgent")
    static String USER_AGENT = null;

    static {
        Thread.setDefaultUncaughtExceptionHandler(new CatchRuntimeException());
        InputStream in = HttpUtil.class.getClassLoader().getResourceAsStream(CONFIG_FILE);
        Properties properties = new Properties();
        try {
            properties.load(in);
            ReflectUtil.loadConfig(HttpUtil.class, null, properties);
            in.close();
        } catch (Exception e) {
            Log.w(e);
        }
    }

    public static Document doGet(String url) throws IOException {
        return Jsoup.connect(url).userAgent(USER_AGENT).get();
    }

    private static class CatchRuntimeException implements Thread.UncaughtExceptionHandler {
        public void uncaughtException(Thread t, Throwable e) {
            return;
        }
    }
}