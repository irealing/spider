package cn.cnysa.spider.util;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 线程池
 *
 * @author Memory_Leak
 *         Created by ireal on 15-7-9.
 */
//TODO 线程池
public class ThreadPool {
    private static ExecutorService service = null;

    static {
        service = Executors.newFixedThreadPool(10);
    }

    private ThreadPool() {
    }
}