package cn.cnysa.spider.util;

import java.lang.annotation.*;
import java.lang.reflect.Field;
import java.util.Properties;

/**
 * 相关参数配置注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface ConfigField {
    ConfigFieldType type() default ConfigFieldType.STRING;

    String value();
}

enum ConfigFieldType {
    STRING, INTEGER, FLOAT, DOUBLE;
}

class ReflectUtil {
    public static <T> void loadConfig(Class<?> clazz, T t, Properties properties) {
        Log.i("加载配置文件");
        Field[] fields = clazz.getDeclaredFields();
        for (Field f : fields) {
            if (!f.isAnnotationPresent(ConfigField.class))
                continue;
            ConfigField cfield = f.getAnnotation(ConfigField.class);
            String value = properties.getProperty(cfield.value());
            try {
                f.set(t, value);
            } catch (Exception e) {
                Log.e(e);
                return;
            }
        }
    }
}