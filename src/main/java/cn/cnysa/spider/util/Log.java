package cn.cnysa.spider.util;

import org.apache.log4j.Logger;

/**
 * @author Memory_Leak
 *         Created by ireal on 15-7-2.
 */
public class Log {
    private final static Logger logger = Logger.getLogger(Log.class);

    public static <T> void d(T t) {
        logger.debug(t);
    }

    public static <T> void w(T t) {
        logger.warn(t);
    }

    public static <T> void w(T msg, Throwable throwable) {
        logger.warn(msg, throwable);
    }

    public static <T> void i(T t) {
        logger.info(t);
    }

    public static <T> void e(T t) {
        logger.error(t);
    }

    public static <T> void e(String msg, Throwable throwable) {
        logger.error(msg, throwable);
    }
}