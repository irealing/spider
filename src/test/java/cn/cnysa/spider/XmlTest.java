package cn.cnysa.spider;

import cn.cnysa.spider.logic.Spider;
import junit.framework.TestCase;

import java.util.List;

/**
 * @author Memory_Leak
 *         Created by ireal on 15-7-9.
 */
public class XmlTest extends TestCase {
    public void testRead() throws Exception {
        List<Spider> spiders = Spider.parseXml("spider.xml");
        System.out.println(spiders.size());
        spiders.get(0).run();
    }
}