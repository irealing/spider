package cn.cnysa.spider;

import cn.cnysa.spider.data.Article;
import cn.cnysa.spider.data.Property;
import cn.cnysa.spider.logic.Pager;
import junit.framework.TestCase;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

/**
 * @author Memory_Leak
 *         Created by ireal on 15-7-2.
 */
public class SpiderTest extends TestCase {

    public void testPager() throws IOException {
        Pager.Builder builder = new Pager.Builder();
        Property title = new Property("title", ".pages-title");
        Property content = new Property("content", ".pages_content");
        builder.url("http://new.sousuo.gov.cn/column/19769/0.htm").items("ul.list01 > li > a").clazz("cn.cnysa.spider.data.Article");
        builder.orm(content);
        builder.orm(title);
        Pager pager = builder.build();
        pager.prepared();
        List<Article> articles = pager.items(Article.class);
        System.out.println("list length:\t" + articles.size());
        Iterator<Article> iterator = articles.iterator();
        while (iterator.hasNext()) {
            Article temp = iterator.next();
            System.out.println(temp.getTitle());
        }
    }
}